//objects
//how to make objects
var person = {
    name: "ashish",
    age: 23,
    testFunction: function (aName) {
      return "Hello," + aName
    }

};

console.log(person.name);
console.log(person.testFunction("deepak"));
//objects as arrays
console.log("Name:" + person["name"]);
person.age += 5;
console.log(person.age);
//function objects

var Vehicle = function(name, color,premium) {
  var self = this;
  // assign
  this.name = name;
  this.odometer = 0;
  this.engin ="V6";
  this.color = color;

  if (premium == true){
    this.engin = "v8";
    this.color="Platinum"
  }

  this.addTrip = function(newMiles){
    if (newMiles > 0){
      self.odometer += newMiles;
    }
  };
};

var benz = new Vehicle("testcaasesds", "white", false);
console.log("color:" + benz.color);
console.log("Odometer:" + benz.odometer);
benz.addTrip(5000);
console.log("Odometer:" + benz.odometer);
benz.addTrip(-5000);
console.log("Odometer:" + benz.odometer);

var volvo = new Vehicle("Volvo", "ugly maroom" , true);
console.log("volvoColor: "  + volvo.color);
 console.log("color: " + volvo["color"]);
