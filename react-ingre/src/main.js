var React = require("react");
var ReactDOM = require('react-dom');
var ListManager = require('./components/ListManager.js');

ReactDOM.render(<ListManager title="ingredients" />, document.getElementById('ingredients'));
