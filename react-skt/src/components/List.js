var React = require('react');
var ListItem = require('./ListItem.js');
var ingredients = [{
                      "id":1,
                      "text": "ham"
                    },
                    {
                      "id":2,
                      "text": "cheese"
                    },
                    {
                      "id":3,
                      "text": "potatoes"
                    }
                  ];
var List = React.createClass({
    render: function() {
      var listItems = ingredients.map(function(item) {
        return <ListItem Key={item.id} ingredient={item.text} />;
      });
      return(
        <ul className="ingredient-list">{listItems}</ul>
      );

    }
});
module.exports = List;
