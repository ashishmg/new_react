var React = require('react');
var ListItem = React.createClass({
  render: function(){
    return (
      <li className="ingredient-list-item">
        <h4 className="ingredient">{this.props.ingredient}</h4>
      </li>
    );
  }
});
module.exports = ListItem;
